# VOXL Python 3.6.9
Python 3.6.9 compiled for VOXL.

### Installing the IPK
1. Once you have the IPK built on your machine, push it to the VOXL target.
```bash
$ adb shell mkdir -p /home/root/ipk
$ adb push Python_3.6.9_8x96.ipk /home/root/ipk/.
```
2. Install the IPK using *opkg*.
```bash
$ opkg install Python_3.6.9_8x96.ipk
```
3. Test that Python3 was installed correctly.
```bash
$ python3 --version
```
If the IPK was installed correctly, you should see the following output:
```bash
Python 3.6.9
```

### Installing numpy for Python3
1. Modify default ar from *busybox* to the regular linux ar, *arm-oemllib32-linux-gnueabi-ar*.
```bash
$ rm /bin/ar
$ ln -s /usr/bin/arm-oemllib32-linux-gnueabi-ar /bin/ar
```

2. Enable Station mode on VOXL in order to connect to the internet.
```bash
$ voxl-wifi station <ssid> [password]
```

3. Use *pip3* to install *numpy*
```bash
$ CFLAGS=-std=c99 pip3 install numpy
```