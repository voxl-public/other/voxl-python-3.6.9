#!/bin/bash
################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

set -e  # exit on error

echo "[INFO] build stage started"

#-------------------------------------------------------------------------------
# Sanity Check
#
# Make sure that we are in the VOXL emulator by checking version (installed).
#-------------------------------------------------------------------------------

if command -v version > /dev/null 2>&1 /dev/null; then
    VERSION_OUTPUT=$(version 2> /dev/null)
    case $VERSION_OUTPUT in
        *ModalAI*)
        echo "USING voxl-docker emulator";;
        *)
        echo "Not running ModalAI Image, program terminated."
        exit 1
    esac

else
    echo "This script must be run in the voxl-docker emulator!"
    exit 1
fi
#-------------------------------------------------------------------------------

ROOT_DIR=$(pwd)
DEST_DIR=$ROOT_DIR/ipk/data

PYTHON_VERSION=Python-3.6.9
PYTHON_TAR=$PYTHON_VERSION.tar.xz
PYTHON_ARCHIVE_URL=https://www.python.org/ftp/python/3.6.9/Python-3.6.9.tar.xz
PYTHON_DIR=$PYTHON_VERSION

#-------------------------------------------------------------------------------
# Get and unpack the Python archive if needed
#-------------------------------------------------------------------------------

if [ ! -d $PYTHON_DIR ]; then
    if [ ! -f $PYTHON_TAR ]; then
       wget $PYTHON_ARCHIVE_URL
    fi

    # default unpack into PYTHON_DIR
    tar xf $PYTHON_TAR   
fi

#-------------------------------------------------------------------------------
# Configure Python
#    The --prefix parameter is set during .configure and cannot be changed
#    during "make install".  --prefix set the install path for make install.
#-------------------------------------------------------------------------------

cd $PYTHON_DIR
echo "cd $PWD"

#echo "configure --preifix=/usr"
./configure --prefix=/usr CFLAGS=-fPIC CXXFLAGS=-fPIC

echo "building..."
make -j8


if [ ! -d $DEST_DIR ]; then
    mkdir $DEST_DIR
fi

echo "installing..."
make DESTDIR=$DEST_DIR install 

# There is a package conflict with lib32-python-dev installed
# But lib32-python-dev can't be uninstalled, so we need to remove the conflicting
# file and add them back via post init
#
rm $DEST_DIR/usr/lib/pkgconfig/python3.pc
rm $DEST_DIR/usr/bin/python3
rm $DEST_DIR/usr/bin/python3-config

